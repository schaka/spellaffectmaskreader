#ifndef MENU_H
#define MENU_H

#include "defines.h"
#include "includes.h"
#include <set>
#include "DBCStructure.h"

enum StringOutputOption
{
    OUTPUT_OPTION_FULL  = 1,
    OUTPUT_OPTION_NAMES = 2
};

class Menu
{
    public:
        typedef std::set<SpellEntry const*> MatchedSpellEntrys;

        void BuildMainMenu();
        void BuildSubMenu(uint8_t option);
        void BuildResultOutput(uint64_t finalMask, uint32_t spellFamilyName);
        void BuildStringOutput(MatchedSpellEntrys matchedSpellEntrys, StringOutputOption outputOption);
        void CheckCurrentMaskValue();
        void CheckSpellFamilyName();

    private:
        uint64_t _currentMaskValue;
        uint16_t _spellFamilyName;
};

#endif
