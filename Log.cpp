#include "defines.h"
#include "includes.h"
#include "Log.h"

Log::Log()
{
    Initialize();
}

Log::~Log()
{
    if (_logFile != NULL)
        fclose(_logFile);

    _logFile = NULL;
}

void Log::Initialize()
{
    _logFileName = "Result.txt";
    _logsDir = "Logs/";

    _logFile = fopen((_logsDir + _logFileName).c_str(), "w");
}

void Log::outResult(const char* str)
{
    fputs(str, _logFile);
}