#include <stdint.h>
#include <string>
#include <list>
#include "DBCStores.h"
#include "DBCfmt.h"

DBCStorage <SpellEntry>                         sSpellStore(SpellEntryfmt);

typedef std::list<std::string> StoreProblemList;

template<class T>
inline void LoadDBC(StoreProblemList& errlist, DBCStorage<T>& storage, const std::string& dbc_path, const std::string& filename)
{
    std::string dbc_filename = dbc_path + filename;
    if (!storage.Load(dbc_filename.c_str()))
    {
        // sort problematic dbc to (1) non compatible and (2) non-existed
        FILE * f = fopen(dbc_filename.c_str(), "rb");
        if (f)
        {
            char buf[100];
            errlist.push_back(dbc_filename + buf);
            fclose(f);
        }
        else
            errlist.push_back(dbc_filename);
    }
}

void LoadDBCStores()
{
    std::string dbcPath = "DBC/";

    StoreProblemList bad_dbc_files;

    LoadDBC(bad_dbc_files, sSpellStore, dbcPath, "Spell.dbc");
}
