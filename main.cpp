#include "defines.h"
#include "includes.h"
#include "menu.h"
#include "DBCStores.h"

using namespace std;

int main()
{
    // Load DBC into storrage
    LoadDBCStores();

    Menu menu;
    menu.BuildMainMenu();
    return 0;
}

