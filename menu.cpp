#include "defines.h"
#include "includes.h"
#include "menu.h"
#include "util.h"
#include <vector>
#include "Log.h"

using namespace std;

void Menu::BuildStringOutput(MatchedSpellEntrys matchedSpellEntrys, StringOutputOption outputOption)
{
    stringstream ss;
    std::vector<string> spellNames;

    for (MatchedSpellEntrys::const_iterator itr = matchedSpellEntrys.begin(); itr != matchedSpellEntrys.end(); ++itr)
    {
        switch (outputOption)
        {
            case OUTPUT_OPTION_FULL:
            { 
                ss << "SpellInfo for <SpellID: " << (*itr)->Id << ">" << endl
                    << "SpellEntry: " << (*itr)->Id << endl
                    << "SpellMask: " << maskToHex((*itr)->SpellFamilyFlags) << endl
                    << "SpellName: " << (*itr)->SpellName[0] << endl << endl;

                break;
            }
            case OUTPUT_OPTION_NAMES:
            {
                spellNames.push_back((*itr)->SpellName[0]);
                break;
            }
            default:
                break;
        }
    }

    if (outputOption == OUTPUT_OPTION_NAMES)
    {
        // Sort vector and remove duplicate strings
        std::sort(spellNames.begin(), spellNames.end());
        spellNames.erase(std::unique(spellNames.begin(), spellNames.end(), stringCompare), spellNames.end());

        for (uint32_t i = 0; i < spellNames.size(); ++i)
            ss << spellNames[i] << endl;
    }
    
    cout << ss.str();

    Log outLog;
    outLog.outResult(ss.str().c_str());
}

void Menu::BuildResultOutput(uint64_t finalMask, uint32_t spellFamilyName)
{
    ostringstream oss;
    std::set<SpellEntry const*> matchedSpellEntrys;

    for (uint32_t i = 0; i < sSpellStore.GetNumRows(); ++i)
    {
        SpellEntry const* spellInfo = sSpellStore.LookupEntry(i);
        if (spellInfo)
        {
            if (!spellInfo->SpellFamilyFlags || !spellInfo->SpellFamilyName)
                continue;

            if (spellInfo->SpellFamilyName == spellFamilyName && (finalMask & spellInfo->SpellFamilyFlags))
                matchedSpellEntrys.insert(spellInfo);
        }
    }

    BuildStringOutput(matchedSpellEntrys, StringOutputOption(1));
    matchedSpellEntrys.clear();

    this_thread::sleep_for(chrono::seconds(10));
}

void Menu::BuildSubMenu(uint8_t option)
{
    _ASSERT(option);

    switch (option)
    {
        case 1:
        {
            CheckCurrentMaskValue();
            CheckSpellFamilyName();
            BuildResultOutput(_currentMaskValue, _spellFamilyName);
            break;
        }
        default:
            break;
    }
}

void Menu::CheckCurrentMaskValue()
{
    cout << "CurrentMaskValue: ";
    cin >> _currentMaskValue;
    cout << endl;

    if (_currentMaskValue == 0)
    {
        cout << "Please enter a positive value above zero." << endl;
        cout << endl;
        CheckCurrentMaskValue();
    }
}

void Menu::CheckSpellFamilyName()
{
    cout << "SpellFamilyName [0 - for help]: ";
    cin >> _spellFamilyName;
    cout << endl;

    if (_spellFamilyName == 0)
    {
        cout << endl;
        cout << "SPELLFAMILY_MAGE:            3" << endl;
        cout << "SPELLFAMILY_WARRIOR:         4" << endl;
        cout << "SPELLFAMILY_WARLOCK:         5" << endl;
        cout << "SPELLFAMILY_PRIEST:          6" << endl;
        cout << "SPELLFAMILY_DRUID:           7" << endl;
        cout << "SPELLFAMILY_ROGUE:           8" << endl;
        cout << "SPELLFAMILY_HUNTER:          9" << endl;
        cout << "SPELLFAMILY_PALADIN:        10" << endl;
        cout << "SPELLFAMILY_SHAMAN:         11" << endl;
        cout << "SPELLFAMILY_PET:            17" << endl << endl;
        CheckSpellFamilyName();
    }
}

void Menu::BuildMainMenu()
{
    cout << "spellAffectMaskReader by robinsch" << endl;
    cout << "(https://bitbucket.org/robinsch/spellaffectmaskreader/)" << endl;
    cout << endl;

    BuildSubMenu(1);
}